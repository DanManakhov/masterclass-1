﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "StickLight Settings")]
public class StickLightSettings : ScriptableObject {

    //public
    public Color color;

    [MinMaxRange(1f, 20f)]
    public RangedFloat Intensity;

    [MinMaxRange(0.1f, 3f)]
    public RangedFloat Duration;

    //private


    int intensity_direction = 1;
    float cur_duration;
    float next_target_duration;


    void SetNewDuration() {
        next_target_duration = Random.Range(Duration.minValue, Duration.maxValue);
    }


    public void AjustSettings(Light light_component, ParticleSystem particle) {

        cur_duration = Mathf.Clamp( cur_duration + intensity_direction*Time.deltaTime, 0, next_target_duration);

        //setting light intensity
        light_component.intensity = Mathf.Lerp(Intensity.minValue, Intensity.maxValue, cur_duration / next_target_duration);

        //setting particle color
        var main = particle.main;
        main.startColor = color;

        //setting light color
        light_component.color = color;


        if (cur_duration >= next_target_duration) 
            intensity_direction = -intensity_direction;
        else
        if (cur_duration <= 0) {
            intensity_direction = -intensity_direction;
            SetNewDuration();
        }
    }


    private void OnEnable()
    {
       SetNewDuration();
    }



}
