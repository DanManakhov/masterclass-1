﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "MouseLook Settings")]
public class MouseLookSettings : ScriptableObject {

    [Range(1f, 10f)]
    public float MouseSens = 5F;



    public float minX = -360;
    public float maxX = 360;

    public float minY = -60;
    public float maxY = 60;


    public bool inverseMouse = true;



    public static MouseLookSettings _instance;

    public static MouseLookSettings Instance {
        get {
            if (!_instance)
                _instance = Resources.Load<MouseLookSettings>("SO_Assets/MouseLookSettings");


            return _instance;
        }
    }


    public float ClampX(float value) {
        return Helpers.ClampAngle(value, minX, maxX);
    }

    public float ClampY(float value) {
        return Helpers.ClampAngle(value, minY, maxY);
    }


    public float InverseParam() {
        return inverseMouse ? -1 : 1;
    }

}
