﻿using UnityEngine;


[CreateAssetMenu( menuName = "Throw Thing")]
public class ThrowThing : ScriptableObject {

    public GameObject[] things;

    [MinMaxRange(0, 100)]
    public RangedFloat force;

    [MinMaxRange(0, 100)]
    public RangedFloat angular_force;





    public GameObject Throw(Vector3 position, Vector3 direction, Collider parent_colllider) {

        GameObject go = Instantiate(things[Random.Range(0, things.Length)], position, Random.rotation);
        Rigidbody rbody = go.GetComponent<Rigidbody>();
        Collider col = go.GetComponent<Collider>();

        Physics.IgnoreCollision(parent_colllider, go.GetComponent<Collider>());


        if (rbody) {
            rbody.AddForce(direction * Random.Range(force.minValue, force.maxValue));
            rbody.AddTorque(new Vector3(Random.Range(angular_force.minValue, angular_force.maxValue), Random.Range(angular_force.minValue, angular_force.maxValue), Random.Range(angular_force.minValue, angular_force.maxValue)));
        }

        return go;
    }

}
