﻿using System.Collections;
using UnityEngine;
using System.Threading;



[CreateAssetMenu(menuName = "Multi Audio Event")]
public class MultiAudioEvent : AudioEvent
{
    public AudioClip[] clips;

    public RangedFloat volume;

    [MinMaxRange(0, 2)]
    public RangedFloat pitch;

    [MinMaxRange(0f, 1f)]
    public RangedFloat Delay;



    public override void Play(GameObject parent)
    {
        if (clips.Length == 0) return;

        AudioSource source = parent.GetComponent<AudioSource>();
        MonoBehaviour mono = parent.GetComponent<MonoBehaviour>();

        if (source && mono)
            if (Application.isPlaying)
                mono.StartCoroutine(PlayAll(source));
            else
                PlayAllEditor(source);
    }


    void PlayAllEditor(AudioSource source) {

        for (int i = 0; i < clips.Length; i++)
        {
            AudioClip clip = clips[Random.Range(0, clips.Length)];

            source.volume = Random.Range(volume.minValue, volume.maxValue);
            source.pitch = Random.Range(pitch.minValue, pitch.maxValue);

            source.PlayOneShot(clip);

            Thread.Sleep( (int)(Random.Range(Delay.minValue, Delay.maxValue) * 1000));
        }
    }


    IEnumerator PlayAll(AudioSource source)
    {
        for ( int i = 0; i < clips.Length; i++)  
        {
            AudioClip clip = clips[Random.Range(0, clips.Length)];

            source.volume = Random.Range(volume.minValue, volume.maxValue);
            source.pitch = Random.Range(pitch.minValue, pitch.maxValue);

            source.PlayOneShot(clip);

            yield return new WaitForSeconds(Random.Range(Delay.minValue, Delay.maxValue));
        }
    }
}