﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockHits : MonoBehaviour {

    public SimpleAudioEvent ground_event;
    public SimpleAudioEvent wall_event;
    public SimpleAudioEvent wood_event;


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.relativeVelocity.magnitude < 1)
            return;

        switch (collision.transform.tag) {
            case "ground":
                if (ground_event)
                    ground_event.Play(gameObject);
                break;

            case "wall":
                if (wall_event)
                    wall_event.Play(gameObject);
                break;

            default:
                if (wood_event)
                    wood_event.Play(gameObject);
                break;
        }

    }
}
