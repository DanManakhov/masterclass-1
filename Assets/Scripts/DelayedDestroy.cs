﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DelayedDestroy : MonoBehaviour {

    public float Delay;
    public bool DoDestroy;

	// Use this for initialization
	void Start () {

        if (DoDestroy)
            Destroy(gameObject, Delay);
	}
	
}
