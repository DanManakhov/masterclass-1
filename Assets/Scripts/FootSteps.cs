﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootSteps : MonoBehaviour {

    public SimpleAudioEvent audio_event;

    private const float step_distance = 0.6f;
    Vector3 last_pos;


    void CheckAndPlay() {
        if (Vector3.Distance(last_pos, transform.position) >= step_distance) {
            last_pos = transform.position;

            if (audio_event)
                audio_event.Play(this.gameObject);
        }
    }


	// Use this for initialization
	void Start () {
        last_pos = transform.position;
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        CheckAndPlay();
	}
}
