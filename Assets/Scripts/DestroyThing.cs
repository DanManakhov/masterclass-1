﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyThing : MonoBehaviour {

    public GameObject ReplaceFor;
    public MultiAudioEvent multi_audio;


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "rock")
            ReplaceSelf();
    }


    void ReplaceSelf() {

        if (ReplaceFor) {
            GameObject go = Instantiate(ReplaceFor, transform.position, transform.rotation);

            if (multi_audio)
                multi_audio.Play(go);
        }

        Destroy(gameObject);
    }
}
