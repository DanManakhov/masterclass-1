﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowRock : MonoBehaviour {

    public ThrowThing throw_thing;
    public SimpleAudioEvent audio_event;

    public GameObject aim_object;


    void Throw() {

        if (!aim_object) return;

        if (throw_thing) {
            Vector3 direction = aim_object.transform.forward;
            Vector3 position = aim_object.transform.position + direction * .5f;

            GameObject go = throw_thing.Throw(position, direction, GetComponent<Collider>());

            

            if (audio_event)
                audio_event.Play(gameObject);
        }

    }


    // Update is called once per frame
    void Update () {
        if (Input.GetButtonDown("Fire1"))
            Throw();

	}
}
