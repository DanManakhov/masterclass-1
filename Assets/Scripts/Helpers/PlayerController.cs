﻿using UnityEngine;
using System.Collections.Generic;



public class PlayerController : MonoBehaviour
{
    [HideInInspector]
    public RaycastHit rayhit;

    public float crosshairAngularSize = 3f;
    private float maxRayDistance = 7f;

    public float crosshairDefaultDepth = 50f;
    public float crosshairMaxDepth = 100f;

    public float unscaledCrosshairDiameter;


    //crosshair stuff
    [Header("Camera")]
    public GameObject CrosshairCanvas = null;

    public GameObject cameraObject;
    public GameObject cameraVRObject;
    public GameObject cameraBoxObject;

    public bool CameraUseController = true;


    //rigidbody
    Rigidbody rbody;


    //buttons
    protected bool UseTouch = false;

    [HideInInspector]
    public bool ButtonsEnabled = true;


    [HideInInspector]
    public bool LeftButton = true;
    [HideInInspector]
    public bool RightButton = true;
    [HideInInspector]
    public bool FireButton = true;
    [HideInInspector]
    public bool FireButtonHold = true;
    [HideInInspector]
    public bool FireInAir = true;



    //rotation
    Quaternion originalRotation;


    private Quaternion fControlRotation;
    public Quaternion ControlRotation {
        get { return fControlRotation; }
        set {

            fControlRotation = value;

            if (cameraObject && CameraUseController)
                cameraObject.transform.localRotation = fControlRotation;
        }
    }


    public void ResetRotation(Quaternion new_rotation) {
        rotationX = 0f;
        rotationY = 0F;

        rotArrayX.Clear();
        rotAverageX = 0F;

        rotArrayY.Clear();
        rotAverageY = 0F;

        if (cameraObject)
            cameraObject.transform.localRotation = new_rotation;

        originalRotation = new_rotation;
    }


    //buttons & axises
    protected bool fire1Pressed = false;
    protected bool fire2Pressed = false;

    protected float lastHAxisValue;


    protected float fire1PressedTime = 0;
    protected float fire2PressedTime = 0;

    float fire1Cooldown; // = 0.1f;


    virtual protected void OnFire1() { }
    virtual protected void OnFire2() { }

    virtual protected void OnFire1Down() { }
    virtual protected void OnFire2Down() { }

    virtual protected void OnFire1Up() { }
    virtual protected void OnFire2Up() { }

    virtual protected void OnVertAxis(float axis) { }
    virtual protected void OnHorizAxis(float axis) { }


    //mouse look
    [Header("Mouse look settings")]

    
    float rotationX = 0F;
    float rotationY = 0F;

    private List<float> rotArrayX = new List<float>();
    float rotAverageX = 0F;

    private List<float> rotArrayY = new List<float>();
    float rotAverageY = 0F;

    float frameCounter = 5;




    //**************************
    // function Implementation

    public void Posess() {
        try {
            //camera
            //cameraObject = GetComponentInChildren<Camera>().gameObject;

            
        } catch (System.Exception e) {
            Debug.Log("Posess: cannot detect Camera script!");
            Debug.Log(e.Message);

        }
    }


    void UpdateMouseLook()
    {
        if (Cursor.lockState != CursorLockMode.Locked) return;

        rotAverageY = 0f;
        rotAverageX = 0f;

        rotationY += Input.GetAxis("Mouse Y") * MouseLookSettings.Instance.MouseSens * MouseLookSettings.Instance.InverseParam();
        rotationX += Input.GetAxis("Mouse X") * MouseLookSettings.Instance.MouseSens;

        rotArrayY.Add(rotationY);
        rotArrayX.Add(rotationX);

        if (rotArrayY.Count >= frameCounter)
        {
            rotArrayY.RemoveAt(0);
        }
        if (rotArrayX.Count >= frameCounter)
        {
            rotArrayX.RemoveAt(0);
        }

        for (int j = 0; j < rotArrayY.Count; j++)
        {
            rotAverageY += rotArrayY[j];
        }
        for (int i = 0; i < rotArrayX.Count; i++)
        {
            rotAverageX += rotArrayX[i];
        }

        rotAverageY /= rotArrayY.Count;
        rotAverageX /= rotArrayX.Count;

        rotAverageY = MouseLookSettings.Instance.ClampY(rotAverageY);
        rotAverageX = MouseLookSettings.Instance.ClampX(rotAverageX);

        Quaternion yQuaternion = Quaternion.AngleAxis(rotAverageY, Vector3.left);
        Quaternion xQuaternion = Quaternion.AngleAxis(rotAverageX, Vector3.up);

        ControlRotation = originalRotation * xQuaternion * yQuaternion;
      
    }


    private void DrawCrosshair()
    {
        if (!this.CrosshairCanvas) return;

        float crosshairDepth = crosshairDefaultDepth;


        if (Physics.Raycast(cameraObject.transform.position, cameraObject.transform.forward, out rayhit, maxRayDistance))
        {
            crosshairDepth = Mathf.Min( crosshairMaxDepth, rayhit.distance );
        }

        //processing crosshair
        CrosshairCanvas.transform.localPosition = new Vector3(0.0f, 0.0f, crosshairDepth);

        // Calculate size that would be required in order have the target angular size
        float desiredSize = Mathf.Tan(crosshairAngularSize * Mathf.Deg2Rad * 0.5f) * 2 * crosshairDepth;
        // Find and set required scale
        float requiredScale = desiredSize / unscaledCrosshairDiameter;
        CrosshairCanvas.transform.localScale = new Vector3(requiredScale, requiredScale, requiredScale);
    }


    void ServeButtons()
    {
        if (!ButtonsEnabled) return;

        if (FireButton)
            if (OVRInput.GetDown(OVRInput.Button.One) || OVRInput.GetDown(OVRInput.RawButton.A) || Input.GetButtonDown("Fire1"))
            {
                UseTouch = (OVRInput.GetActiveController() == OVRInput.Controller.RTouch);
                //Debug.Log(string.Format("Use touch: {0}", UseTouch));


                fire1Pressed = true;
                OnFire1Down();
            }


        if (OVRInput.GetDown(OVRInput.RawButton.B))
        {
            fire2Pressed = true;
            OnFire2Down();
        }


        if (OVRInput.GetUp(OVRInput.Button.One) || OVRInput.GetUp(OVRInput.RawButton.A) || Input.GetButtonUp("Fire1"))
        {
            fire1Pressed = false;
            fire1PressedTime += Time.deltaTime;

            OnFire1Up();
            fire1PressedTime = 0;
        }


        if (OVRInput.GetUp(OVRInput.RawButton.B))
        {
            fire2Pressed = false;
            fire2PressedTime += Time.deltaTime;

            OnFire2Up();
            fire2PressedTime = 0;
        }


        if (fire1Pressed)
        {
            fire1PressedTime += Time.deltaTime;
            OnFire1();
        }


        if (fire2Pressed)
        {
            fire2PressedTime += Time.deltaTime;
            OnFire1();
        }
    }


    void ServeAxis() {
        int ax = 0;
        int ay = 0;

        // ***********************
        // *** Horizontal Axis ***
        // ***********************

        if (RightButton && LeftButton) {
            
            Vector2 axis = OVRInput.Get(OVRInput.RawAxis2D.RThumbstick);
            //Debug.Log(axis);
            ax = Mathf.RoundToInt(axis.x*10)/10;  //get discrete value
            //Debug.Log(ax);
        }

        if (RightButton && OVRInput.GetDown(OVRInput.Button.DpadRight))
            ax = 1;

        if (LeftButton && OVRInput.GetDown(OVRInput.Button.DpadLeft))
           ax = -1;


        // ***********************
        // *** Horizontal Axis ***
        // ***********************

        if (OVRInput.GetDown(OVRInput.Button.DpadUp) || Input.GetKeyDown(KeyCode.UpArrow))
            ay = 1;

        if (OVRInput.GetDown(OVRInput.Button.DpadDown) || Input.GetKeyDown(KeyCode.DownArrow))
            ay = -1; 


        // Call events

        if (ax != 0) OnHorizAxis(ax);
        if (ay != 0) OnVertAxis(ay);

        lastHAxisValue = ax;
    }


    private void Move()
    {
        rbody.angularVelocity = Vector3.zero;

        Vector3 targetForward = ControlRotation * Vector3.forward;
        Vector3 targetRight = ControlRotation * Vector3.right;

        float axis_vert = Input.GetAxis("Vertical");
        float axis_horiz = Input.GetAxis("Horizontal");


        float f_Speed = 1.2f;


        Vector3 move_forward = targetForward * axis_vert;
        Vector3 move_side = targetRight * axis_horiz ;

        move_forward = Vector3.ClampMagnitude(move_forward + move_side, 1f) * f_Speed * Time.deltaTime;

        // Apply this movement to the rigidbody's position.
        rbody.MovePosition(rbody.position + move_forward);
    }


    void LockCursor() {

        if (Input.GetButtonDown("Fire1")) {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }


    // Update is called once per frame
    protected virtual void FixedUpdate () {
        LockCursor();
        UpdateMouseLook();

        ServeButtons();
        ServeAxis();

        Move();

        DrawCrosshair();
    }


    protected virtual void Start()
    {
        ResetRotation(Quaternion.identity);


        rbody = GetComponent<Rigidbody>();


        if (CrosshairCanvas != null)
            CrosshairCanvas.SetActive(true);
    }

 
}
