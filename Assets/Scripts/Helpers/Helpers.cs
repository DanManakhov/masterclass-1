﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum GameMode { Single = 1, Multi = 2 };


public static class Helpers
{

    public static GameObject M_GetChild(this MonoBehaviour item, string name)
    {
        Transform[] children = item.GetComponentsInChildren<Transform>(true);

        foreach (Transform child in children)
        {
            if (child.name == name)
                return child.gameObject;
        }

        return null;
    }



    public static bool ExistsIn<InType>(this InType item, params InType[] items)
    {
        bool result_flag = false;


        if (items != null)
            foreach (InType it in items)
                if (it.Equals(item))
                {
                    result_flag = true;
                    break;
                }

        return result_flag;
    }


    public static float Frac(float value) {
        return value - (int)value;
    }


    public static float ClampAngle(float angle, float min, float max)
    {
        angle = angle % 360;
        if ((angle >= -360F) && (angle <= 360F))
        {
            if (angle < -360F)
            {
                angle += 360F;
            }
            if (angle > 360F)
            {
                angle -= 360F;
            }
        }
        return Mathf.Clamp(angle, min, max);
    }


}
