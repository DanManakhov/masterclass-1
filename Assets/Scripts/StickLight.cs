﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickLight : MonoBehaviour {

    public StickLightSettings settings;


    Light light_component;
    ParticleSystem particle;

    private void Start()
    {
        light_component = GetComponentInChildren<Light>();

        particle = transform.Find("fx_fire").GetComponent<ParticleSystem>();
    }

    // Use this for initialization
    void Update () {
        if (settings && light_component && particle)
            settings.AjustSettings(light_component, particle);
	}
	
}
